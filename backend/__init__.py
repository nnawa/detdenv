from eve import Eve
from flask import render_template
import backend.app_auth
import backend.images_control
import logging


def create_app(test_config=None):
    app = Eve(__name__,
                # auth=app_auth.AppAuth,
                static_folder='./templates/static')


    @app.after_request
    def after_request(response):
        response.headers.set('Access-Control-Allow-Headers', 'Content-Type,Authorization,If-Match')
        return response

    # event and database hooks
    app.on_insert_accounts += app_auth.create_user
    # app.on_update_images += images_control.single_label_fix
    app.on_pre_POST_com += images_control.pre_POST_com
    # app.on_post_GET_pconf += images_control.post_GET_pconf

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    @app.route('/')
    def index():
        return render_template('index.html')

    # app logger for detecting something something
    handler = logging.FileHandler('app.log')
    handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(filename)s:%(lineno)d] -- ip: %(clientip)s, '
        'url: %(url)s, method:%(method)s'))
    app.logger.setLevel(logging.INFO)
    app.logger.addHandler(handler)

    return app
