import numpy as np
from flask import current_app as app
import cvxopt.solvers
from sklearn.naive_bayes import GaussianNB
from sklearn.externals import joblib
# import cv2
#
# class StatModel(object):
#     def load(self, fn):
#         self.model.load(fn)  # Known bug: https://github.com/Itseez/opencv/issues/4969
#     def save(self, fn):
#         self.model.save(fn)
#
# class SVM(StatModel):
#     def __init__(self):
#         self.model = cv2.ml.SVM_create()
#         self.model.setKernel(cv2.ml.SVM_LINEAR)
#         self.model.setType(cv2.ml.SVM_C_SVC)
#
#     def train(self, samples, labels):
#         self.model.train(samples, cv2.ml.ROW_SAMPLE, labels)
#
#     def predict(self, samples):
#         return self.model.predict(samples)[1].ravel()


def makeLabel(labels):
    return np.array(labels)


def makeSample(samples):
    return np.array(samples)


def train(samples, labels):
    X, y = samples, labels
    m = X.shape[0]

    # Gram matrix - The matrix of all possible inner products of X.
    K = np.array([np.dot(X[i], X[j])
                 for j in range(m)
                 for i in range(m)]).reshape((m, m))
    P = cvxopt.matrix(np.outer(y, y) * K)
    q = cvxopt.matrix(-1 * np.ones(m))

    # Equality constraints
    A = cvxopt.matrix(y, (1, m), 'd')
    b = cvxopt.matrix(0.0)

    # Inequality constraints
    G = cvxopt.matrix(np.diag(-1 * np.ones(m)))
    h = cvxopt.matrix(np.zeros(m))

    # Solve the problem
    solution = cvxopt.solvers.qp(P, q, G, h, A, b)

    # Lagrange multipliers
    multipliers = np.ravel(solution['x'])

    # app.logger.info("\nP:{}\n q:{}\n G:{}\n h:{}\n A:{}\n b:{}\n multipliers:{}\n".format(P, q, G, h, A, b, multipliers))
    # Support vectors have positive multipliers.
    has_positive_multiplier = multipliers > 1e-7
    sv_multipliers = multipliers[has_positive_multiplier]

    support_vectors = X[has_positive_multiplier]
    support_vectors_y = y[has_positive_multiplier]

    def compute_w(multipliers, X, y):
        return np.sum(multipliers[i] * X[i] * y[i] for i in range(len(y)))

    w = compute_w(sv_multipliers, support_vectors, support_vectors_y)

    def compute_b(w, X, y):
        return np.sum(y[i] - np.dot(w, X[i]) for i in range(len(X))) / len(X)

    b = compute_b(w, support_vectors, support_vectors_y)

    # app.logger.info("\nw: {}\nb: {}\n X: {}\n support_vectors: {}\n has_positive_multiplier: {}".format(w, b, X, support_vectors, sv_multipliers))

    return w, b, support_vectors, support_vectors_y


def test(samples, model):
    w, b, a, y = model
    result = np.zeros(len(samples), np.int8)

    def hypothesize(w, b, x, a, y):
        return 1 if np.dot(x, w) + b >= 1e-7 else -1
        # return 1 if np.sum(a[i] * y[i] * np.dot(x, w) + b) >= 1e-7 else -1

    for i in range(len(result)):
        result[i] = hypothesize(w, b, samples[i], a, y)

    return result


def train_NBC(samples, labels):
    nbc = GaussianNB()
    nbc.fit(samples, labels)
    joblib.dump(nbc, 'nbc_model.sav')


def test_NBC(samples, model):
    nbc = joblib.load('nbc_model.sav')
    nbc.set_params(model)
    return nbc.predict(samples)
