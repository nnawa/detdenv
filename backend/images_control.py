from flask import current_app as app
from bson.objectid import ObjectId
from gridfs import GridFS
from datetime import datetime
# from requests_toolbelt import MultipartEncoder
import backend.img_processing as imp
import backend.classifier as csf
import requests as rq
# import uuid
import json

# import numpy as np

# API_HOST = 'http://127.0.0.1:5000/api'

img_size = {
    'height': 1200,
    'width': 1600
    }
small_cell = 0.002083 * img_size['height'] * img_size['width']
circularity_thresh = 0.75
erosion_kernel = 17
dilation_kernel = 17
median_blur_kernel = 77
segmentation_kernel = 333
# img_size = {
#     'height': 1200,
#     'width': 1600
#     }
# small_cell = 0.002083 * img_size['height'] * img_size['width']
# circularity_thresh = 0.75
# erosion_kernel = 17
# dilation_kernel = 17
# median_blur_kernel = 77
# segmentation_kernel = 333
last_vector = [245, 225, 224]

# def single_label_fix(updates, original):
#     app.logger(updates)
#     if type(updates['labels']) is int:
#         updates['labels'] = [items['labels']]

def pre_POST_com(request):
    images = app.data.driver.db['images']
    pconf = app.data.driver.db['pconf'].find()[0]
    file_storage = GridFS(app.data.driver.db)

    headers = {
    'Authorization': request.headers.get('Authorization'),
    }
    api_host = request.url_root+'api/'

    commands = request.form.getlist('command')
    scopes = request.form.getlist('scope')
    ref_ids = request.form.getlist('ref_id')

    #app.logger.info(request.json)

    if request.is_json:
        commands = [x['command'] for x in request.json]
        scopes = [x['scope'] for x in request.json]
        ref_ids = [x['ref_id'] for x in request.json] if commands.count('train_model') == 0 else [None]

    for i, command in enumerate(commands):
        scope = scopes[i]
        ref_id = ref_ids[i]
        image = images.find_one({'_id': ObjectId(ref_id)}) if ref_id is not None else None

        if command == 'process_image' and scope == ['single']:
            r = single_image_process(image, headers, api_host, ref_id, file_storage, pconf)

        elif (command == 'post_steps' or command == 'put_steps') and scope == ['single']:
            r = single_stepped_process(image, headers, api_host, ref_id, file_storage, command, pconf)

        elif command == 'train_model':
            r = train_model(images, headers, api_host, file_storage, pconf)

        elif command == 'test_model':
            r = test_model(images, headers, api_host, ref_id, file_storage, pconf)

        elif command == 'analyze':
            r = analyze(images, headers, api_host, ref_id)
            # app.logger.info(r.text)

        elif scope[0] == 'single' and (command == 'post_histogram' or command == 'put_histogram'):
            r = compute_histogram(image, headers, api_host, ref_id, file_storage, scope, command, pconf)


def train_model(images, headers, api_host, file_storage, pconf):
    labels = []
    cell_hists = []
    request = []

    for image in images.find(filter={'is_processed': True}, projection={'labels': 1, 'cells': 1, '_etag': 1, 'raw_file': 1}):
        if list(image.keys()).count('labels') == 0:
            continue
        headers, img, item_url = image_preparation(image, headers, api_host, str(image['_id']), file_storage, pconf)
        labels.extend(image['labels'])
        cells = imp.base64toContour(image['cells'])
        # app.logger.info(cells[0].shape)
        cell_hists.extend(imp.cellHistograms(cells, img))
        request.append(rq.patch(item_url, headers=headers, data={'is_trained': True}))

    labels = csf.makeLabel(labels)
    samples = csf.makeSample(cell_hists)
    #app.logger.info(len(labels))
    #app.logger.info(len(samples))

    w, b, a, y = csf.train(samples, labels)
    app.logger.info(csf.train_NBC(samples, labels))
    now = [str(x) for x in datetime.now().timetuple()]

    data = {
        'name': 'model-{}-{}-{}-{}:{}:{}'.format(now[0], now[1], now[2], now[3], now[4], now[5]),
        'w': imp.contourToBase64([w]),
        'b': b,
        'a': imp.contourToBase64([a]),
        'y': imp.contourToBase64([y])
    }

    request.append(rq.post(api_host+'svm_model/', data=data))

    return request


def test_model(images, headers, api_host, ref_id, file_storage, pconf):
    request = []
    svm_model = app.data.driver.db['svm_model'].find_one({'_id': ObjectId(ref_id)})

    for image in images.find(filter={'is_trained': False}, projection={'raw_file': 1, 'filename':1, '_etag': 1, 'is_processed': 1, 'cells': 1}):
        headers, img, item_url = image_preparation(image, headers, api_host, str(image['_id']), file_storage, pconf)
        if not image['is_processed']:
            cells, cell_count, detected = process_image(img, pconf)
            cells = imp.base64toContour(cells)
        else:
            cells = imp.base64toContour(image['cells'])
        # app.logger.info(len(cells))
        cell_hists = imp.cellHistograms(cells, img)
        result = csf.test(cell_hists, (
            imp.base64toContour(svm_model['w'])[0],
            svm_model['b'],
            imp.base64toContour(svm_model['a'])[0],
            imp.base64toContour(svm_model['y'])[0]
        ))

        #app.logger.info(result.tolist())
        detected = imp.labelMarkImages(cells, img, labels=result)
        detected = imp.imageToBuffer(detected)

        data = {
            'result': result.tolist(),
            'cells': imp.contourToBase64(cells),
            'is_tested': True,
            # 'is_trained': True
        }

        req = rq.patch(item_url, headers=headers, json=data)
        headers['If-Match'] = req.json()['_etag']

        files = {'detected': (image['filename'].split('.')[0]+'_processed.jpg', detected)}

        request.append(rq.patch(item_url, headers=headers, files=files))

    return request


def image_preparation(image, headers, api_host, ref_id, file_storage, pconf):
    headers['If-Match'] = image['_etag']

    raw_img_id = image['raw_file']
    raw_img = file_storage.get(raw_img_id).read()
    img = imp.bufferToImage(raw_img)


    if img.shape[0] != pconf['image_size']['height'] or img.shape[1] != pconf['image_size']['width']:
        img = imp.resize_image(img, (pconf['image_size']['width'], pconf['image_size']['height']))

    item_url = api_host+'images/'+ref_id
    return headers, img, item_url


def compute_histogram(image, headers, api_host, ref_id, file_storage, scope, command, pconf):
    headers, img, item_url = image_preparation(image, headers, api_host, ref_id, file_storage, pconf)
    # hist_db = app.data.drivers.db['histogram']
    # if command == 'put_histogram':
    #     hist_query = hist_db.find_one(filter={'ref_id': ObjectId(ref_id), 'groups': scope[1:]})
    # else:
    #     hist_query = None

    request = []
    resource_url = api_host+'histogram/'
    b, g, r = imp.histogram(img, 256, normal_area=img.shape[0]*img.shape[1])
    scope += ['image']
    request.append(send_histogram(command, b, g, r, ref_id, resource_url, scope, headers))
    h, s, v = imp.histogram(imp.convertToHSV(img), 256, normal_area=img.shape[0]*img.shape[1], interval=[[0,180], [0,255], [0,255]])
    request.append(send_histogram(command, h, s, v, ref_id, resource_url, scope, headers))
    # gray = imp.medianBlur(imp.gs_projection(img, last_vector), pconf['median_kernel'])
    # grayh = list(imp.histogram(gray, 256, img_dim=1, interval=[[0,255]]))[0]
    # request.append(send_histogram(command, grayh, grayh, grayh, ref_id, resource_url, scope, headers))

    cells, cell_count, processed = process_image(img, pconf)
    cells = imp.base64toContour(cells)
    mask = imp.rasterizeContours(cells, img)
    h, s, v = imp.histogram(imp.convertToHSV(img), 16, mask=mask, normal_area=mask.sum()/255, interval=[[0,180], [0,255], [0,255]])
    scope = scope[:-1] + ['cells']
    request.append(send_histogram(command, h, s, v, ref_id, resource_url, scope, headers))
    b, g, r = imp.histogram(img, 16, normal_area=img.shape[0]*img.shape[1])
    request.append(send_histogram(command, b, g, r, ref_id, resource_url, scope, headers))

    files = {'detected': (image['filename'].split('.')[0]+'_processed.jpg', processed)}
    request.append(rq.patch(item_url, headers=headers, files=files))

    for cell in cells:
        mask = imp.rasterizeContours([cell], img)
        h, s, v = imp.histogram(imp.convertToHSV(img), 16, mask=mask, normal_area=mask.sum()/255, interval=[[0,180], [0,255], [0,255]])
        scope = scope[:-1] + ['cell']
        request.append(send_histogram(command, h, s, v, ref_id, resource_url, scope, headers))

    return request


def send_histogram(command, h, s, v, ref_id, resource_url, scope, headers, hist_query=None):
    h = h.flatten().tolist()
    s = s.flatten().tolist()
    v = v.flatten().tolist()
    request = []

    data = [
        {'hist': h, 'ref_id': ObjectId(ref_id), 'groups': (['blue'] if scope[-1] == 'image' else ['hue'])+scope[1:]},
        {'hist': s, 'ref_id': ObjectId(ref_id), 'groups': (['green'] if scope[-1] == 'image' else ['saturation'])+scope[1:]},
        {'hist': v, 'ref_id': ObjectId(ref_id), 'groups': (['red'] if scope[-1] == 'image' else ['value'])+scope[1:]}
    ]

    if command == 'post_histogram':
        for datum in data:
            request.append(rq.post(resource_url, headers=headers, data=datum))
        return request
    else:
        hist_db = app.data.driver.db['histogram']
        for datum in data:
            hist_query = hist_db.find_one(filter={'ref_id': ObjectId(ref_id), 'groups': datum['groups']})
            hist_id = hist_query['_id']
            hist_headers = headers.copy()
            hist_headers['If-Match'] = hist_query['_etag']
            request.append(rq.put(resource_url+str(hist_id), headers=hist_headers, data=datum))
        return request


def analyze(images, headers, api_host, ref_id):
    svm_model = app.data.driver.db['svm_model'].find_one({'_id': ObjectId(ref_id)})
    result = []
    labels = []

    for image in images.find(filter={'is_tested': True}, projection={'labels': 1, 'result': 1}):
        result.extend(image['result'])
        labels.extend(image['labels'])

    tp, tn, fp, fn = imp.confusion_matrix(labels, result)
    data = {
        'true_positive': tp, 'true_negative': tn,
        'false_positive': fp, 'false_negative': fn
    }

    headers['If-Match'] = svm_model['_etag']

    # app.logger.info(headers)

    return rq.patch(api_host+'svm_model/'+ref_id, headers=headers, data=data)


def single_image_process(image, headers, api_host, ref_id, file_storage, pconf):
    headers, img, item_url = image_preparation(image, headers, api_host, ref_id, file_storage, pconf)

    cells, cell_count, result = process_image(img, pconf)

    data = {
        'cells': cells,
        'is_processed': True,
        'labels': [0 for x in range(cell_count)] #if cell_count > 1 else 0,
    }

    req = rq.patch(item_url, headers=headers, json=data)
    headers['If-Match'] = req.json()['_etag']

    files = {'detected': (image['filename'].split('.')[0]+'_processed.jpg', result)}

    # app.logger.info(data)
    return rq.patch(item_url, headers=headers, files=files)


def single_stepped_process(image, headers, api_host, ref_id, file_storage, command, pconf):
    headers, img, item_url = image_preparation(image, headers, api_host, ref_id, file_storage, pconf)
    stepped_db = app.data.driver.db['steps']
    # app.logger.info(app.data.driver.db['steps'].find_one(filter={'ref_id': ObjectId(ref_id)})['ref_id'])
    if command == 'put_steps':
        # stepped_images = [step for step in stepped_db.find(filter={'ref_id': ObjectId(ref_id)})]
        stepped_images = stepped_db.find(filter={'ref_id': ObjectId(ref_id)})

    processed_images = step_by_step_process(img, pconf)

    resource_url = api_host+'steps/'

    request = []

    for i, result in enumerate(processed_images):
        result = imp.imageToBuffer(result)
        data = {
            'step': i,
            'ref_id': ObjectId(ref_id)
        }
        files = {'image': (image['filename'].split('.')[0]+"_step_{}.jpg".format(i), result)}
        if command == 'post_steps':
            request.append(rq.post(resource_url, headers=headers, data=data, files=files))
        else:
            step_id = stepped_images[i]['_id']
            step_headers = headers.copy()
            step_headers['If-Match'] = stepped_images[i]['_etag']
            request.append(rq.put(resource_url+str(step_id), headers=step_headers, data=data, files=files))

    data = {'is_stepped': True}
    request.append(rq.patch(item_url, headers=headers, data=data))

    return request


def process_image(img, pconf):
    smallest_cell = pconf['smallest_cell'] * pconf['image_size']['height'] * pconf['image_size']['width']
    gsp = imp.gs_projection(img, last_vector)
    blur = imp.medianBlur(gsp, pconf['median_kernel'])
    if pconf['segmentation_method'] == 'adaptive':
        th = imp.adaptiveThreshold(blur, kernel_size=pconf['segmentation_kernel'])
    else:
        retval, th = imp.otsuThreshold(blur)
    eroded = imp.erode(th, pconf['erode_kernel'])
    dilated = imp.dilate(eroded, pconf['dilate_kernel'])
    _contour = imp.detectContours(dilated)
    cells_cnt, _dirty = imp.finalCleaning(
        _contour,
        blur,
        min_area=smallest_cell,
        circularity_th=pconf['circularity_th']
    )
    # cells_cnt.extend(imp.extendedCleaning(
    #     _dirty,
    #     blur,
    #     min_area=smallest_cell,
    #     circularity_th=pconf['circularity_th']
    # ))
    result = imp.labelMarkImages(cells_cnt, img)
    # app.logger.info(cells_cnt)
    # imp.showImage(result)
    return imp.contourToBase64(cells_cnt), len(cells_cnt), imp.imageToBuffer(result)


def step_by_step_process(img, pconf):
    smallest_cell = pconf['smallest_cell'] * pconf['image_size']['height'] * pconf['image_size']['width']

    gsp = imp.gs_projection(img, last_vector)
    blur = imp.medianBlur(gsp, pconf['median_kernel'])
    if pconf['segmentation_method'] == 'adaptive':
        th = imp.adaptiveThreshold(blur, kernel_size=pconf['segmentation_kernel'])
    else:
        retval, th = imp.otsuThreshold(blur)
    eroded = imp.erode(th, pconf['erode_kernel'])
    dilated = imp.dilate(eroded, pconf['dilate_kernel'])
    _contour = imp.detectContours(dilated)
    _cleaned, _dirty = imp.finalCleaning(_contour, blur, min_area=smallest_cell, circularity_th=pconf['circularity_th'])
    # app.logger.info("{},{}".format(len(_cleaned), len(_dirty)))
    _cleaned.extend(imp.extendedCleaning(_dirty, blur, min_area=smallest_cell, circularity_th=pconf['circularity_th']))
    circularized = imp.rasterizeContours(_cleaned, img)
    result = imp.labelMarkImages(_cleaned, img)
    # result = imp.andImage(img, img, masking=circularized)
    stub = imp.cellHistograms(_cleaned, img)
    # app.logger.info(imp.adaptiveThreshold(blur, kernel_size=5)[207:213,508:514])
    # app.logger.info(blur[207:212,508:513].mean())

    return (gsp, blur, 255-th, 255-eroded, 255-dilated, 255-circularized, result-1)
