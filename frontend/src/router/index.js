import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Training from '@/components/Training'
import Testing from '@/components/Testing'
import Inspect from '@/components/Inspect'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/training',
      name: 'Training',
      component: Training
    },
    {
      path: '/inspect',
      name: 'Inspect',
      component: Inspect
    },
    {
      path: '/testing',
      name: 'Testing',
      component: Testing
    }
  ]
})
