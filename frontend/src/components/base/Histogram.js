import { Line, Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  props: ['datasets'],
  mounted() {
    this.renderChart({
      labels: this.datasets.labels,
      datasets: this.datasets.datasets
    }, {responsive: true, maintainAspectRatio: false})
  }
}
